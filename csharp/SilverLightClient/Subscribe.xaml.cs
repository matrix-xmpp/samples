﻿using System.Windows;
using System.Windows.Controls;
using Matrix;
using Matrix.Xmpp.Client;

namespace SilverLightClient
{
    /// <summary>
    /// 
    /// </summary>
    public partial class Subscribe : UserControl
    {
        Jid             _jid;
        XmppClient      _xmppClient;
        PresenceManager _presenceManager;

        #region << Constructor >>
        /// <summary>
        /// Initializes a new instance of the <see cref="Subscribe"/> class.
        /// </summary>
        /// <param name="xmppClient">The XMPP client.</param>
        /// <param name="jid">The jid.</param>
        /// <param name="inRoster">if set to <c>true</c> [in roster].</param>
        public Subscribe(XmppClient xmppClient, Jid jid, bool inRoster)
        {
            InitializeComponent();
            
            _jid = jid;
            _xmppClient = xmppClient;
            _presenceManager = new PresenceManager(_xmppClient);
            
            runJid.Text = jid;
            
            if (inRoster)
                panelAdd.Visibility = Visibility.Collapsed;
            else
                chkAddContact.IsChecked = true;
        }
        #endregion

        /// <summary>
        /// Handles the Click event of the cmdApprove control to approve the subscription request.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void cmdApprove_Click(object sender, RoutedEventArgs e)
        {
            _presenceManager.ApproveSubscriptionRequest(_jid);

            if (chkAddContact.IsChecked == true)
            {
                new RosterManager(_xmppClient).Add(_jid, txtNickname.Text);
                _presenceManager.Subscribe(_jid);
            }

            Close();
        }

        /// <summary>
        /// Handles the Click event of the cmdDeny control to deny the subscription request.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void cmdDeny_Click(object sender, RoutedEventArgs e)
        {
            _presenceManager.DenySubscriptionRequest(_jid);
            this.Close();
        }

        /// <summary>
        /// Closes this window.
        /// </summary>
        private void Close()
        {
            ((PopupWindow)this.Parent).Close();
        }
    }
}