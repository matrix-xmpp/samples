﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

namespace Server
{
    /// <summary>
    /// This is a very basic server example which shows you how to get started with server programming in MatriX.
    /// To write a server which is ready for production much more work has to be done based on this example. And you
    /// also have to study the XMPP RFCs 6120 and 6121 in detail if you want to write a compliant server.
    /// 
    /// This server example does not have any connection to a database for users and contacts.
    /// * The hardcoded xmpp domain is localhost
    /// * user1 to user10 exist
    /// * all users have teh password secret
    /// * all users have each other on the contact list
    /// 
    /// This means you can use any xmpp client and try to connect with eg user1@localhost and the password secret.
    /// </summary>
    public partial class FrmServer : Form
    {
        // Thread signal.
        private readonly ManualResetEvent allDone = new ManualResetEvent(false);
        private Socket m_Listener;
        private bool m_Listening;

        
        /// <summary>
        /// Main entry point of the application
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new FrmServer());
        }

        public FrmServer()
        {
            InitializeComponent();
            SetLicense();
        }

        /// <summary>
        /// Sets the license and activate the evaluation.
        /// </summary>
        private static void SetLicense()
        {
            // request demo license here:
            // http://www.ag-software.net/matrix-xmpp-sdk/request-demo-license/
            const string LIC = @"YOUR LICENSE";
            Matrix.License.LicenseManager.SetLicense(LIC);

            // when something is wrong with your license you can find the error here
            Console.WriteLine(Matrix.License.LicenseManager.LicenseError);
        }

        private void cmdStart_Click(object sender, EventArgs e)
        {
            var myThreadDelegate = new ThreadStart(Listen);
            var myThread = new Thread(myThreadDelegate);
            myThread.Start();
        }

        private void cmdStop_Click(object sender, EventArgs e)
        {
            m_Listening = false;
            allDone.Set();
        }

        private void Listen()
        {
            var localEndPoint = new IPEndPoint(IPAddress.Any, 5222);

            // Create a TCP/IP socket.
            m_Listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);


            // Bind the socket to the local endpoint and listen for incoming connections.
            try
            {
                m_Listener.Bind(localEndPoint);
                m_Listener.Listen(10);

                m_Listening = true;

                while (m_Listening)
                {
                    // Set the event to nonsignaled state.
                    allDone.Reset();

                    // Start an asynchronous socket to listen for connections.
                    Console.WriteLine("Waiting for a connection...");
                    m_Listener.BeginAccept(AcceptCallback, null);

                    // Wait until a connection is made before continuing.
                    allDone.WaitOne();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public void AcceptCallback(IAsyncResult ar)
        {
            // Signal the main thread to continue.
            allDone.Set();
            // Get the socket that handles the client request.
            var sock = m_Listener.EndAccept(ar);

            var con = new XmppSeverConnection(sock);
        }
    }
}
