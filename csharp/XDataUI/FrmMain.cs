﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Matrix.Xml;
using Matrix.Xmpp.XData;

namespace XDataUI
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void cmdRenderUI_Click(object sender, EventArgs e)
        {
            var xdata = XmppXElement.LoadXml(txtFormXml.Text, true) as Data;
            if (xdata != null)
                xDataControl1.CreateForm(xdata);
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            string xml = @" <x xmlns='jabber:x:data' type='form'>
    <title>Voice request</title>
    <instructions>
      To approve this request for voice, select 
      the &quot;Grant voice to this person?&quot;
      checkbox and click OK. To skip this request, 
      click the cancel button.
    </instructions>
    <field var='FORM_TYPE' type='hidden'>
        <value>http://jabber.org/protocol/muc#request</value>
    </field>
    <field var='muc#role'
           type='text-single'
           label='Requested role'>
      <value>participant</value>
    </field>
    <field var='muc#jid'
           type='jid-single'
           label='User ID'>
      <value>hag66@shakespeare.lit/pda</value>
    </field>
    <field var='muc#roomnick'
           type='text-single'
           label='Room Nickname'>
      <value>thirdwitch</value>
    </field>
    <field var='muc#request_allow'
           type='boolean'
           label='Grant voice to this person?'>
      <value>false</value>
    </field>
  </x>";

            string xml2 = @"<x xmlns='jabber:x:data' type='form'>
      <title>Dark Cave Registration</title>
      <instructions>
        Please provide the following information
        to register with this room.
      </instructions>
      <field
          type='hidden'
          var='FORM_TYPE'>
        <value>http://jabber.org/protocol/muc#register</value>
      </field>
      <field
          label='Given Name'
          type='text-single'
          var='muc#register_first'>
        <required/>
      </field>
      <field
          label='Family Name'
          type='text-single'
          var='muc#register_last'>
        <required/>
      </field>
      <field
          label='Desired Nickname'
          type='text-single'
          var='muc#register_roomnick'>
        <required/>
      </field>
      <field
          label='Your URL'
          type='text-single'
          var='muc#register_url'/>
      <field
          label='Email Address'
          type='text-single'
          var='muc#register_email'/>
      <field
          label='FAQ Entry'
          type='text-multi'
          var='muc#register_faqentry'/>
    </x>";
            txtFormXml.Text = xml2;
        }

        private void xDataControl1_OnOk(object sender, EventArgs e)
        {
            var respXData = xDataControl1.CreateResponse();
            txtResultXml.Text = respXData.ToString();

            tabControl.SelectedTab = tabResult;
        }
    }
}
